import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BasicSelenium {
    WebDriver driver;

    @BeforeEach
    public void setUp(){
        System.setProperty("webdriver.chromedriver.driver", "chromedriver");
        driver = new ChromeDriver();

    }

    @Test

    public void BasicSelenium(){
        driver.get("https://flight-order.herokuapp.com");

        driver.findElement(By.cssSelector("body > div > h1")).getAttribute("textContent");
        driver.findElement(By.id("flight_form_firstName")).sendKeys("Honza");
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("London");
        driver.findElement(By.cssSelector("body > div > div > form > div > button")).click();

    }

    @Test
    public void flightFormTest(){

        String firstname = "Honza";
        String lastname = "Novak";
        driver.get("https://flight-order.herokuapp.com");

        //fill first_name
        driver.findElement(By.id("flight_form_firstName")).sendKeys("Honza");
        //fill surname
        driver.findElement(By.id("flight_form_lastName")).sendKeys("Novak");
        //fill email
        driver.findElement(By.id("flight_form_email")).sendKeys("honz@gmail.com");
        //fill birthDay
        driver.findElement(By.id("flight_form_birthDate")).sendKeys("2000-06-14");
        //send form
        driver.findElement(By.id("flight_form_submit")).click();

        WebElement nameBox = driver.findElement(By.cssSelector("body tr:first-child td"));

        Assertions.assertEquals(firstname + " " + lastname, nameBox.getAttribute("textContent"));
    }

}
