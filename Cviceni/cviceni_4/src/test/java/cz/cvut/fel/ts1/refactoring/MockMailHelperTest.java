package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

/**
 * mock(DBManager.class)
 * when(methodCall) – umožňuje definovat chování dané metody (Stub konfigurace)
 * when(mockedDbManager.findMail(anyInt())).thenReturn(mailToReturn);
 * verify(mock) – umožňuje ověřit, zda metoda mocku byla volána (i dle parametrů)
 * verify(mockedDbManager,times(2)).saveMail(any(Mail.class));
 */

public class MockMailHelperTest {

    @Test
    public void sendMail_validId_mailWillBeSaved() {
        DBManager dbManager = mock(DBManager.class);
        MailHelper mailHelper =  new MailHelper(dbManager);
        int mailId = 1;
        Mail mailToReturn = new Mail();
        when(dbManager.findMail(anyInt())).thenReturn(mailToReturn);

        mailHelper.sendMail(mailId);

        verify(dbManager).findMail(mailId);
        //verify(dbManager,times(1)).findMail(mailId);
    }
}
