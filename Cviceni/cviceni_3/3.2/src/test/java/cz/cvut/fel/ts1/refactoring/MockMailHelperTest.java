package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class MockMailHelperTest {

        @Test
        public void sendMail_validId_mailWillBeSave(){
            DBManager dbManager = mock(DBManager.class);
            MailHelper mailHelper = new MailHelper(dbManager);
            int mailId = 1;

            mailHelper.sendMail(mailId);
            verify(dbManager).findMail(mailId);
            //verify(dbManager, times(wantedNumberOfInvocations:1)).findMail();

        }

}
