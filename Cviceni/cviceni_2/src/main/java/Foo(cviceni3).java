public class Foo {
    private int num = 0;

    public int returnNumber() {
        return 5;
    }

    public int getNum() {
        return num;
    }

    public void increment() {
        num++;
    }

    public int sum(int a, int b){
        return a+b;
    }

    public void exceptionThrown() throws Exception {
        throw new Exception("Ocekavana vyjimka");
    }
}
