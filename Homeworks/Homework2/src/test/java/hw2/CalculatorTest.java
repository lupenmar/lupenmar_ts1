package hw2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    private Calculator calculator;
    int c = 0;
    int a = 3;
    int b = 5;

    @BeforeEach
    public void hello(){
        calculator = new Calculator();
    }

    @Test
    public void testAdd(){
        int res = calculator.add(3,6);
        Assertions.assertEquals(9, res);
    }

    @Test
    public void testAdd2(){
        int res = calculator.add(-3, 3);

        Assertions.assertEquals(0, res);
    }

    @Test
    public void testSubtract(){
        double expectedValue = 10;
        double res = calculator.subtract(20,10);
        Assertions.assertEquals(expectedValue, res);
    }

    @Test
    public void testSubtract2(){
        double expectedValue = 15;
        double res = calculator.subtract(10, -5);

        Assertions.assertEquals(expectedValue, res);
    }

    @Test
    public void testMultiply(){
        double expectedValue = 15;

        double res = calculator.multiply(3 , 5);

        Assertions.assertEquals(expectedValue, res);

    }

    @Test
    public void testMultiply2() {
        double expectedValue = 0;

        double res = calculator.multiply(5, 0);

        Assertions.assertEquals(expectedValue, res);

    }

    @Test
    public void testDivide() throws Exception {
        double expectedValue = a / b;

        double res = calculator.divide(a , b);

        Assertions.assertEquals(expectedValue, res);
    }

    @Test
    public void testDivide1() {
        Assertions.assertThrows(Exception.class, () -> calculator.divide(a , c));
    }

}
