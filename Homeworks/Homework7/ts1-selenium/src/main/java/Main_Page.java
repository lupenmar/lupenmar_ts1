import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Main_Page {
    private final WebDriver driver;

    @FindBy(className = "register-link")
    private WebElement rgstrLink;

    public Main_Page(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);
    }

    public Main_Page(WebDriver driver, String queryString) {
        this.driver = driver;
        driver.get(queryString);

        PageFactory.initElements(driver, this);

        // accept cookies
        try {
            driver.findElement(By.cssSelector("button[data-cc-action='accept']")).click();
        } catch (NoSuchElementException e) {}
    }

    public void clickRegisterLink() {
        rgstrLink.click();
    }

    public void clickAdvancedSearchLink() {
        driver.findElement(By.className("open-search-options")).click();
        driver.findElement(By.id("advanced-search-link")).click();
    }
}



