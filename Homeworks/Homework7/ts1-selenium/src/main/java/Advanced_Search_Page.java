import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Advanced_Search_Page {
    private final WebDriver driver;

    @FindBy(id = "all-words")
    private WebElement allWords;

    @FindBy(id = "least-words")
    private WebElement leastWords;

    @FindBy(id = "title-is")
    private WebElement titleIs;

    @FindBy(id = "date-facet-mode")
    private WebElement dateFacetMode;

    @FindBy(id = "facet-start-year")
    private WebElement facetStartYear;

    @FindBy(id = "submit-advanced-search")
    private WebElement clicksubmitButton;

    public Advanced_Search_Page(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com/advanced-search");

        PageFactory.initElements(driver, this);
        acceptCookies();

    }

    public Advanced_Search_Page(WebDriver driver, String queryString) {
        this.driver = driver;
        driver.get(queryString);

        PageFactory.initElements(driver, this);

        acceptCookies();
    }

    public void setAllWords(String text) {
        this.allWords.sendKeys(text);
    }

    public void setLeastWords(String text) {
        this.leastWords.sendKeys(text);
    }

    public void setTitleIsWords(String text) {
        this.titleIs.sendKeys(text);
    }

    public void setDateFacetModeToIn() {
        Select select = new Select(this.dateFacetMode);
        select.selectByIndex(1);
    }

    public void setFacetStartYear(String year) {
        this.facetStartYear.clear();
        this.facetStartYear.sendKeys(year);
    }

    public void sendForm() {
        this.clicksubmitButton.click();
    }

    public void acceptCookies() {
        try {
            driver.findElement(By.cssSelector("button[data-cc-action='accept']")).click();
        } catch (NoSuchElementException e) {}
    }
}
