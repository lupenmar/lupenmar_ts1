import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Process_Test {

    private WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterEach
    public void closeWindow() {
        driver.quit();
    }

    @Test
    public void prepareData() throws InterruptedException {
        // Home Page
        Main_Page main_page = new Main_Page(driver, "https://link.springer.com/");
        main_page.clickAdvancedSearchLink();

        // Advanced Search Page
        Advanced_Search_Page advancedSearchPage = new Advanced_Search_Page(driver, driver.getCurrentUrl());
        advancedSearchPage.setAllWords("Page Object Model");
        advancedSearchPage.setLeastWords("Sellenium Testing");
        advancedSearchPage.setDateFacetModeToIn();
        advancedSearchPage.setFacetStartYear(String.valueOf(LocalDate.now().getYear()));
        Thread.sleep(1000);
        advancedSearchPage.sendForm();

        // Search Page
        Search_Page searchPage = new Search_Page(driver, driver.getCurrentUrl());
        searchPage.setContentType("Article");

        ArrayList<String> links = searchPage.getResultsLinks(4);

        // Data of articles
        StringBuilder dataForCSV = new StringBuilder();

        for (String link : links) {
            Article_Page page = new Article_Page(driver, link);
            dataForCSV.append(page.getName()).append(", ");
            dataForCSV.append(page.getDate()).append(", ");
            dataForCSV.append(page.getDOI()).append("\n");
        }

        // Writing data into csv file
        try {
            FileWriter myWriter = new FileWriter("src/main/resources/result.csv");
            myWriter.write(dataForCSV.toString());
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ParameterizedTest(name = "name {0}, date {1}")
    @CsvFileSource(resources = "/result.csv")
    public void parameterizedTest(String name, String date, String DOI) throws InterruptedException {

        // Home page
        Main_Page main_page = new Main_Page(driver, "https://link.springer.com/");
        main_page.clickRegisterLink();

        // Login
        Sign_Up_And_Login signupLoginPage = new Sign_Up_And_Login(driver);
        signupLoginPage.setLoginEmail("mrg.lupenko@gmail.com");
        signupLoginPage.setLoginPassword("margo2411");
        signupLoginPage.sendLoginForm();

        // From home page to search
        main_page.clickAdvancedSearchLink();

        // Search
        Advanced_Search_Page advancedSearchPage = new Advanced_Search_Page(driver);
        advancedSearchPage.setTitleIsWords(name);
        advancedSearchPage.sendForm();

        // Get
        Search_Page searchPage = new Search_Page(driver);

        // Article Page
        Article_Page articlePage = new Article_Page(driver, searchPage.getResultsLinks(1).get(0));
        Thread.sleep(1000);

        assertEquals(name, articlePage.getName());
        assertEquals(date, articlePage.getDate());
        assertEquals(DOI, articlePage.getDOI());
    }

}
