import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Main_Page_Test {

    WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chromedriver.driver", "chromedriver");
        driver = new ChromeDriver();
    }


    @Test
    public void registrationLinkTest() throws InterruptedException {

        String expUrl = "https://link.springer.com/signup-login?previousUrl=https%3A%2F%2Flink.springer.com%2F";

        Main_Page main_page = new Main_Page(driver, "https://link.springer.com/");
        main_page.clickRegisterLink();
        Thread.sleep(1000);

        assertEquals(expUrl, driver.getCurrentUrl());
        driver.close();
    }

    @Test
    public void advancedSearchLinkTest() throws InterruptedException {

        String expUrl = "https://link.springer.com/advanced-search";

        Main_Page main_page = new Main_Page(driver, "https://link.springer.com/");
        main_page.clickAdvancedSearchLink();
        Thread.sleep(1000);

        assertEquals(expUrl, driver.getCurrentUrl());
        driver.close();
    }

}
