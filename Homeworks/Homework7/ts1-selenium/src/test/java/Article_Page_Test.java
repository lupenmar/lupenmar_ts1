import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Article_Page_Test {

    WebDriver driver;

    @BeforeEach
    public void setUp() {

        System.setProperty("webdriver.chromedriver.driver", "chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void getNameTest() throws InterruptedException {

        String expName = "APOGEN: automatic page object generator for web testing";

        Article_Page articlePage = new Article_Page(driver, "https://link.springer.com/article/10.1007/s11219-016-9331-9");
        Thread.sleep(1000);

        assertEquals(expName, articlePage.getName());
        driver.close();
    }

    @Test
    public void getDateTest() throws InterruptedException {

        String expDate = "09 August 2016";

        Article_Page articlePage = new Article_Page(driver, "https://link.springer.com/article/10.1007/s11219-016-9331-9");
        Thread.sleep(1000);

        assertEquals(expDate, articlePage.getDate());
        driver.close();
    }

    @Test
    public void getDOITest() throws InterruptedException {

        String expDOI = "https://doi.org/10.1007/s11219-016-9331-9";

        Article_Page articlePage = new Article_Page(driver, "https://link.springer.com/article/10.1007/s11219-016-9331-9");
        Thread.sleep(1000);

        assertEquals(expDOI, articlePage.getDOI());
        driver.close();
    }
}
