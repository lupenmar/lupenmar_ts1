import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Advanced_Search_Page_Test {

    WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chromedriver.driver", "chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void advancedSearchTest() throws InterruptedException {

        String expUrl = "https://link.springer.com/search?query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29&date-facet-mode=in&facet-start-year=2022&showAll=true";

        Advanced_Search_Page advancedSearchPage = new Advanced_Search_Page(driver);
        advancedSearchPage.setAllWords("Page Object Model");
        advancedSearchPage.setLeastWords("Sellenium Testing");
        advancedSearchPage.setDateFacetModeToIn();
        advancedSearchPage.setFacetStartYear("2022");
        Thread.sleep(1000);
        advancedSearchPage.sendForm();

        assertEquals(expUrl, driver.getCurrentUrl());
        driver.close();
    }

}
