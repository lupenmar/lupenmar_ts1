import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Search_Page_Test {

    WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chromedriver.driver", "chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void setTypeChapterTest() throws InterruptedException {

        String expValue = "within Chapter";

        Search_Page searchPage = new Search_Page(driver, "https://link.springer.com/search/page/1?date-facet-mode=in&facet-start-year=2022&showAll=true&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29");
        searchPage.setContentType("Chapter");
        Thread.sleep(1000);

        assertEquals(expValue, driver.findElement(By.cssSelector(".facet-constraint-message")).getText());
        driver.close();
    }

    @Test
    public void setPageTest() throws InterruptedException {

        String expValue = "5";

        Search_Page searchPage = new Search_Page(driver, "https://link.springer.com/search/page/1?date-facet-mode=in&facet-start-year=2022&showAll=true&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29");
        searchPage.setPage(5);
        Thread.sleep(2000);

        assertEquals(expValue, driver.findElement(By.cssSelector("input[name='go-to-page']")).getAttribute("value"));
        driver.close();
    }

    @Test
    public void getResultsLinksTest() throws InterruptedException {

        Search_Page searchPage = new Search_Page(driver, "https://link.springer.com/search/page/1?date-facet-mode=in&facet-start-year=2022&showAll=true&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29");
        Thread.sleep(1000);

        assertEquals(4, searchPage.getResultsLinks(4).size());
        driver.close();
    }
}
