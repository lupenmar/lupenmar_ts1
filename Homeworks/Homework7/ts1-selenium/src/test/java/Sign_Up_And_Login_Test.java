import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Sign_Up_And_Login_Test {

    WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chromedriver.driver", "chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void loginTest() throws InterruptedException {

        String email = "mrg.lupenko@gmail.com";
        String password = "margo2411";
        String expectedUrl = "https://link.springer.com/";

        Sign_Up_And_Login signupLoginPage = new Sign_Up_And_Login(driver, "https://link.springer.com/signup-login?previousUrl=https%3A%2F%2Flink.springer.com%2F");
        signupLoginPage.setLoginEmail(email);
        signupLoginPage.setLoginPassword(password);
        Thread.sleep(1000);
        signupLoginPage.sendLoginForm();

        assertEquals(expectedUrl, driver.getCurrentUrl());
        driver.close();
    }

}
