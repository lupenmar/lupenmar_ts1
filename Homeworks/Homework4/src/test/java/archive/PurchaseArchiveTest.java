package archive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import static org.mockito.Mockito.*;


public class PurchaseArchiveTest {

    PurchasesArchive purchasesArchive;
    StandardItem standardItem;
    HashMap <Integer, ItemPurchaseArchiveEntry> integerItemPurchaseArchiveEntryHashMap;
    ItemPurchaseArchiveEntry itemPurchaseArchiveEntry;
    ArrayList<Order> orderArrayList;

    @BeforeEach
    public void hello_its_baze(){
        integerItemPurchaseArchiveEntryHashMap = new HashMap<>();
        orderArrayList = new ArrayList<>();
        standardItem = new StandardItem(100, "cat", 3450,"animals",44);
        itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(standardItem);
        integerItemPurchaseArchiveEntryHashMap.put(standardItem.getID(), itemPurchaseArchiveEntry);
        purchasesArchive = new PurchasesArchive(integerItemPurchaseArchiveEntryHashMap, orderArrayList);
    }



    @Test
    public void printItemPurchaseStatisticsTest(){

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        String res = "ITEM PURCHASE STATISTICS:\n";

        System.setOut(printStream);

        res += "ITEM  Item   ID " + standardItem.getID() + "   NAME " + standardItem.getName() + "   CATEGORY " +
                standardItem.getCategory() + "   PRICE " + standardItem.getPrice() + "   LOYALTY POINTS " +
                standardItem.getLoyaltyPoints() + "   HAS BEEN SOLD " + 1 + " TIMES\n";

        purchasesArchive.printItemPurchaseStatistics();
        String out = byteArrayOutputStream.toString();
        Assertions.assertEquals(res, out);
    }

    @Test
    public void getHowManyTimesHasBeenItemSoldTest(){

        Integer cislo = purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem);

        Assertions.assertEquals(itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold(), cislo);
    }

    @Test
    public void putOrderToPurchasesArchiveTest(){

        ShoppingCart cart = new ShoppingCart();
        cart.addItem(standardItem);
        Order order = new Order(cart, "Yana", "Minsk", 10);

        purchasesArchive.putOrderToPurchasesArchive(order);

        Assertions.assertEquals(2, purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem));
    }

    @Test
    public void printlnStreamTest(){
        Iterator i = mock(Iterator.class);
        String text = "";

        Collection<Object> input = new HashMap<>().values();

        for (Object e : input){
            text += e.toString();
        }
        when(i.next()).thenReturn("ITEM PURCHASE STATISTICS:").thenReturn(text);
        String res = i.next()+""+i.next();
        Assertions.assertEquals("ITEM PURCHASE STATISTICS:" + text, res);

    }

    @Test
    public void mockOrderArchiveTest(){
        ArrayList<Order> mockOA = mock(ArrayList.class);
        PurchasesArchive purchA = new PurchasesArchive(integerItemPurchaseArchiveEntryHashMap, mockOA);
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(standardItem);
        Order order = new Order(cart, "Queen", "Castle");

        purchA.putOrderToPurchasesArchive(order);

        verify(mockOA).add(order);
    }

    @Test
    public void mockItemPurchaseArchiveEntryTest(){

        ItemPurchaseArchiveEntry itemPAE = mock(ItemPurchaseArchiveEntry.class);

        itemPAE.getCountHowManyTimesHasBeenSold();

        verify(itemPAE).getCountHowManyTimesHasBeenSold();

    }

    @Test

    public void ItemPurchaseArchiveEntryConstructorTest(){

        ItemPurchaseArchiveEntry itemPAE = new ItemPurchaseArchiveEntry(standardItem);

        Assertions.assertEquals(standardItem.getID(),itemPAE.getRefItem().getID());
        Assertions.assertEquals(standardItem.getName(),itemPAE.getRefItem().getName());
        Assertions.assertEquals(standardItem.getPrice(),itemPAE.getRefItem().getPrice());
        Assertions.assertEquals(standardItem.getCategory(),itemPAE.getRefItem().getCategory());
        Assertions.assertEquals(standardItem.getClass(),itemPAE.getRefItem().getClass());
    }

}
