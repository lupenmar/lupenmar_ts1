package shop;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.mockito.internal.matchers.Or;
import shop.Order;
import shop.ShoppingCart;

import java.util.ArrayList;

public class OrderTest {
    ShoppingCart cart;
    String customerName;
    String customerAddress;
    int state;

    Order order;

    @Test
    public void OrderConstructorTestStatejeNeniNull(){

        ArrayList<Item> objects = new ArrayList<>();

        Item obj1 = new StandardItem(5, "bear", 2400, "toys", 25);
        Item obj2 = new StandardItem(19, "apple", 20, "fruits", 30);

        String customerName = "Elena";
        String customerAddress = "Praha";

        objects.add(obj1);
        objects.add(obj2);

        ShoppingCart cart = new ShoppingCart(objects);

        int state = 6;

        order = new Order(cart, customerName, customerAddress, state);

        Assertions.assertEquals(objects, order.getItems());
        Assertions.assertEquals(customerAddress, order.getCustomerAddress());
        Assertions.assertEquals(customerName, order.getCustomerName());
        Assertions.assertEquals(state, order.getState());
    }

    @Test
    public void OrderConstructorTestStatejeNull(){

        ArrayList<Item> objects = new ArrayList<>();

        Item obj1 = new StandardItem(5, "bear", 2400, "toys", 25);
        Item obj2 = new StandardItem(19, "apple", 20, "fruits", 30);

        objects.add(obj1);
        objects.add(obj2);

        ShoppingCart cart = new ShoppingCart(objects);

        String customerName = "Elena";
        String customerAddress = "Praha";

        order = new Order(cart, customerName, customerAddress);

        Assertions.assertEquals(objects, order.getItems());
        Assertions.assertEquals(customerAddress, order.getCustomerAddress());
        Assertions.assertEquals(customerName, order.getCustomerName());
        Assertions.assertEquals(0, order.getState());
    }

    @Test

    public void OrderConstructorTestnaInvalidHodnoty(){

        Assertions.assertThrows(Exception.class, () -> {

            String customerAddress = "";
            String customerName = "";
            ShoppingCart cart = null;

            int state = 0;

            Order obj1 = new Order(cart, customerName,customerAddress);
            Order obj2 = new Order(cart, customerName,customerAddress);
        });
    }

}
