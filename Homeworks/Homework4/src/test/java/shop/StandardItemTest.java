package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;
import shop.StandardItem;

import java.lang.reflect.Parameter;

public class StandardItemTest {

    int id;
    String name;
    float price;
    String category;
    int loyaltyPoints;
    private Object object;

    @Test
    public void StandardItemConstructTest (){

        id = 3;
        name = "Screwdriver";
        price = 200;
        category = "TOOLS";
        loyaltyPoints = 5;

        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);

        Assertions.assertEquals(id, standardItem.getID());
        Assertions.assertEquals(name, standardItem.getName());
        Assertions.assertEquals(category, standardItem.getCategory());
        Assertions.assertEquals(price, standardItem.getPrice());
        Assertions.assertEquals(loyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @Test
    public void StandardItemCopy(){

        id = 3;
        name = "Screwdriver";
        price = 200;
        category = "TOOLS";
        loyaltyPoints = 5;

        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItemCopy = standardItem.copy();

        Assertions.assertEquals(standardItem, standardItemCopy);

    }

   @ParameterizedTest
   @CsvFileSource(resources = "text.csv")

   public void StandardItemEqualsTest(int id, String name, float price, String category, int loyaltyPoints){

        StandardItem si1 = new StandardItem(id, name, price, category, loyaltyPoints);
       StandardItem si2 = new StandardItem(id, name, price, category, loyaltyPoints);

       Assertions.assertEquals(si1, si2);
   }
}
