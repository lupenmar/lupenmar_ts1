package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import shop.StandardItem;

public class ItemStockTest {

    ItemStock itemStock;

    @Test
    public void ItemStockConstructTest(){

        int count = 0;

        StandardItem obj = new StandardItem(9, "notebook", 3000, "electronics", 40);

        itemStock = new ItemStock(obj);

        Assertions.assertEquals(obj, itemStock.getItem());
        Assertions.assertEquals(count, itemStock.getCount());

    }

    @ParameterizedTest
    @CsvFileSource(resources = "2text.csv")

    public void ItemStockZmenaPocetMistPlus(){

        int num = 24;

        StandardItem obj = new StandardItem(9, "notebook", 3000, "electronics", 40);

        itemStock = new ItemStock(obj);

        int newCislo = itemStock.getCount();

        itemStock.IncreaseItemCount(num);
        Assertions.assertEquals(newCislo + num, itemStock.getCount());

    }

    @ParameterizedTest
    @CsvFileSource(resources = "2text.csv")

    public void ItemStockZmenaPocetMistMinus(){

        int num = 0;

        StandardItem obj = new StandardItem(9, "notebook", 3000, "electronics", 40);

        itemStock = new ItemStock(obj);

        int newCislo = itemStock.getCount();

        itemStock.decreaseItemCount(num);

        Assertions.assertEquals(newCislo - num, itemStock.getCount());

    }
}
